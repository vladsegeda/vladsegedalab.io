import React from 'react';
import "./scss/styles.scss";
import Routing from "./view/components/Router/Router";

function App() {

    return (
        <Routing />
    );
}

export default App;
