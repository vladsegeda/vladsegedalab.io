import React from "react";
import blog2 from "../../../../static/img/blog2.png";

function Preview2() {
    return (
        <div className='blog-container-preview'>
            <div className='blog-image'>
                <img src={blog2} alt={blog2}/>
            </div>
            <div className='blog-content'>
                <h2>Что такое CRM и зачем она нужна</h2>
                <p>Для успеха бизнесу нужны растущие продажи и довольные клиенты. В теории может звучать просто, но как этого добиться? Ответ — CRM-система. В статье рассказываем простыми словами, что такое CRM, зачем она нужна, какие даёт преимущества и возможности....</p>
                <div className='blog-content-meta'>
                    <span>Дата: 05.04.20</span>
                    <a href="/" className='btn-main'>Читать дальше</a>
                </div>
            </div>
        </div>
    )
}

export default Preview2;