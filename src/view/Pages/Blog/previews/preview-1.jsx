import React from "react";
import crm from "../../../../static/img/why.png";

function Preview1() {
    return (
        <div className='blog-container-preview'>
            <div className='blog-image'>
                <img src={crm} alt={crm}/>
            </div>
            <div className='blog-content'>
                <h2>Какую CRM систему выбрать</h2>
                <p>CRM-система (Customer Relationship Management — управление взаимоотношениями с клиентами) облегчает
                    работу сотрудников и помогает увеличить продажи. Как выбрать CRM-систему, чтобы ее внедрение не
                    затянулось на годы и приносило бизнесу доход, а не убытки? ...</p>
                <div className='blog-content-meta'>
                    <span>Дата: 01.04.20</span>
                    <a href="/" className='btn-main'>Читать дальше</a>
                </div>
            </div>
        </div>
    )
}

export default Preview1;