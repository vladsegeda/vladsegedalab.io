import React from "react";
import blog1 from "../../../../static/img/blog3.png";

function Preview3() {
    return (
        <div className='blog-container-preview'>
            <div className='blog-image'>
                <img src={blog1} alt={blog1}/>
            </div>
            <div className='blog-content'>
                <h2>CRM для малого бизнеса: 5 правил успешного внедрения</h2>
                <p>Рассказываем, почему малому бизнесу нужна CRM, какие есть причины для внедрения системы и зачем её внедрять. Часто бывает, что компании малого бизнеса работают без CRM-системы. До некоторых пор всё идет нормально, но как только количество заказов и клиентов начинает расти, возникает много проблем...</p>
                <div className='blog-content-meta'>
                    <span>Дата: 15.04.20</span>
                    <a href="/" className='btn-main'>Читать дальше</a>
                </div>
            </div>
        </div>
    )
}

export default Preview3;