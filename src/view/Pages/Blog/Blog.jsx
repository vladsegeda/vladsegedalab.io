import React from "react";
import Preview1 from "./previews/preview-1";
import Preview2 from "./previews/preview-2";
import Preview3 from "./previews/preview-3";


function Blog() {
    return (
        <div className='blog'>
            <div className='blog-bg'><h1>Блог</h1></div>
            <div className='blog-container'>
                <Preview3 />
                <Preview2 />
                <Preview1 />
            </div>
        </div>
    )
}

export default Blog;