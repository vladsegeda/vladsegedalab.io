import React from 'react';
import Header from "../../components/HomePage/Header/Header";
import Features from "../../components/HomePage/Features/Features";


function Home() {
    return (
        <>
            <Header />
            <Features />
        </>
    );
}

export default Home;
