import React from 'react';

function Navigation(props) {
    const {menu, menuItem} = props;
    return (
            <nav className={menu}>
                <a className={menuItem} href='/'>Главная</a>
                <a className={menuItem} href='/about-us'>О нас</a>
                <a className={menuItem} href='/features'>Особенности</a>
                <a className={menuItem} href='/contact-us'>Контакты</a>
                <a className={menuItem} href='/blog'>Блог</a>
            </nav>
    )
}

export default Navigation;
