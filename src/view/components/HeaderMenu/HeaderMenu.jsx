import React, {useCallback, useState} from 'react';
import {Collapse} from 'react-burgers'
import Navigation from "./Navigation/Navigation";
import SideDrawer from "./SideDrawer/SideDriver";
import Backdrop from "./SideDrawer/Backdrop";

function HeaderMenu() {
    const [active, setActive] = useState(false);

    const menuHandler = useCallback((event) => {
        setActive(prevState => !prevState)
    }, []);

    const backdropClickHandler = useCallback((event) => {
        setActive(false)
    }, []);

    let backdrop;
    if (active) {
        backdrop = <Backdrop click={backdropClickHandler}/>;
    }

    return (
        <header className='header-container'>
            <a href='/'><h1 className='header-container-logo'>MCRM</h1></a>
            <div className="drawer">
                <Collapse
                    width={30}
                    lineHeight={2}
                    lineSpacing={6}
                    color='#0057ef'
                    active={active}
                    onClick={menuHandler}
                />
            </div>
            <SideDrawer
                show={active}
                menu='side-drawer'
            />
            {backdrop}
            <Navigation
                menu='menu'
                menuItem='menu-item'
            />
        </header>
    )
}

export default HeaderMenu;
