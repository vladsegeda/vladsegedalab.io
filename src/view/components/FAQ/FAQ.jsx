import React, {useCallback, useState} from "react";
import Collapse from "@kunukn/react-collapse/dist/Collapse.umd";
import cx from "classnames";

function FAQBlock() {
    const [isOpen1, setIsOpen1] = useState(false);
    const [isOpen2, setIsOpen2] = useState(false);
    const [isOpen3, setIsOpen3] = useState(false);
    const [isOpen4, setIsOpen4] = useState(false);

    const toggle1 = useCallback(() => {
        setIsOpen1(prevState => !prevState);
    },[]);
    const toggle2 = useCallback(() => {
        setIsOpen2(prevState => !prevState);
    },[]);
    const toggle3 = useCallback(() => {
        setIsOpen3(prevState => !prevState);
    },[]);
    const toggle4 = useCallback(() => {
        setIsOpen4(prevState => !prevState);
    },[]);

    return (
        <div className='faq'>
            <span className='faq-subTitle'>FAQ's</span>
            <h2>Frequently Asked Questions</h2>
            <button
                className={cx("faq-toggle", {
                    "faq-toggle--active": isOpen1
                })}
                onClick={() => toggle1()}
            >
                <span className="faq-toggle-text">What is DAAS?</span>
                <div className="rotate90">
                    <svg
                        className={cx("faq-icon", { "faq-icon--expanded": isOpen1 })}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={isOpen1}
                className={
                    "faq-collapse faq-collapse--gradient " +
                    (isOpen1 ? "faq-collapse--active" : "")
               }
            >
                <div className="faq-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </Collapse>
            <button
                className={cx("faq-toggle", {
                    "faq-toggle--active": isOpen2
                })}
                onClick={() => toggle2()}
            >
                <span className="faq-toggle-text">How to porchase?</span>
                <div className="rotate90">
                    <svg
                        className={cx("faq-icon", { "faq-icon--expanded": isOpen2 })}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={isOpen2}
                className={
                    "faq-collapse faq-collapse--gradient " +
                    (isOpen2 ? "faq-collapse--active" : "")
                }
            >
                <div className="faq-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </Collapse>
            <button
                className={cx("faq-toggle", {
                    "faq-toggle--active": isOpen3
                })}
                onClick={() => toggle3()}
            >
                <span className="faq-toggle-text">Can I use for fee?</span>
                <div className="rotate90">
                    <svg
                        className={cx("faq-icon", { "faq-icon--expanded": isOpen3 })}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={isOpen3}
                className={
                    "faq-collapse faq-collapse--gradient " +
                    (isOpen3 ? "faq-collapse--active" : "")
                }
            >
                <div className="faq-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </Collapse>
            <button
                className={cx("faq-toggle", {
                    "faq-toggle--active": isOpen4
                })}
                onClick={() => toggle4()}
            >
                <span className="faq-toggle-text">How to get refond?</span>
                <div className="rotate90">
                    <svg
                        className={cx("faq-icon", { "faq-icon--expanded": isOpen4 })}
                        viewBox="6 0 12 24"
                    >
                        <polygon points="8 0 6 1.8 14.4 12 6 22.2 8 24 18 12"/>
                    </svg>
                </div>
            </button>
            <Collapse
                isOpen={isOpen4}
                className={
                    "faq-collapse faq-collapse--gradient " +
                    (isOpen4 ? "faq-collapse--active" : "")
                }
            >
                <div className="faq-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </div>
            </Collapse>

        </div>
    )
}

export default FAQBlock;