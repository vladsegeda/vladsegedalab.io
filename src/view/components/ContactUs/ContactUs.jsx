import React from "react";
import ContactForm from "../ContactForm/ContactForm";
import contacts from "../../../static/img/contacts.png";

function ContactUs() {
    return (
        <div className='home-features-container'>
            <div className='features-item'>
                <ContactForm
                    title="Остались вопросы?"
                />
            </div>
            <div className='features-item'>
                <div className='features-item-image'>
                    <img src={contacts} alt={contacts}/>
                </div>
            </div>
        </div>
    )
}

export default ContactUs;