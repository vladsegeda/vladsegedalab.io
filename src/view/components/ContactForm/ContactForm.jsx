import React, {useCallback, useState} from 'react';


function ContactForm(props) {
    const {title, text} = props;

    const [userForm, SetUserForm] = useState({
        name: '',
        email: '',
        number: '',
        description: '',
    });

    const onChangeHandler = useCallback((event) => {
        const {name, value} = event.target;
        SetUserForm(prevState => {
            return {
                ...prevState,
                [name]: value
            }
        })
    },[]);

    const onSubmitHandler = useCallback((event) => {
        event.preventDefault();
        console.log(userForm);
    }, [userForm]);

    return (
        <div className='contactForm'>
            <h1>{title}</h1>
            <p>{text}</p>
            <form onSubmit={onSubmitHandler}>
                <input
                    type="text"
                    name="name"
                    placeholder='Имя'
                    onChange={onChangeHandler}
                />
                <input
                    type="email"
                    name="email"
                    placeholder='Email'
                    onChange={onChangeHandler}
                />
                <input
                    type="text"
                    name="number"
                    placeholder='Номер телефона'
                    onChange={onChangeHandler}
                />
                <input
                    type="text"
                    name="description"
                    placeholder='Вопрос'
                    onChange={onChangeHandler}
                />
                <input
                    className="submitButton"
                    type={"submit"}
                    placeholder="submit"
                    value="Отправить"
                />
            </form>
        </div>
    );
}

export default ContactForm;
