import React from "react";
import microsoft from "../../../static/img/logos/microsoft.png";
import airbnb from "../../../static/img/logos/airbnb.png";
import linkedin from "../../../static/img/logos/linkedin.png";
import piedpiper from "../../../static/img/logos/piedpiper.png";


function TrustedBy() {
    return (
        <div className='trustedBy'>
            <h1 className='trustedBy-title'>Нам доверяют крупные корпорации</h1>
            <div className='trustedBy-container'>
                <div className='trustedBy-container-item'><img src={microsoft} alt={microsoft} /></div>
                <div className='trustedBy-container-item'><img src={airbnb} alt={airbnb} /></div>
                <div className='trustedBy-container-item'><img src={linkedin} alt={linkedin} /></div>
                <div className='trustedBy-container-item'><img src={piedpiper} alt={piedpiper} /></div>
            </div>
        </div>
    )
}

export default TrustedBy;