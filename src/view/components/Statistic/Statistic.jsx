import React from "react";

function Statistic() {
    return (
        <div className='statistic'>
            <div className='statistic-item'>
                <p className='statistic-item-value'>2М+</p>
                <p className='statistic-item-description'>Загрузок</p>
            </div>
            <div className='statistic-item'>
                <p className='statistic-item-value'>30К+</p>
                <p className='statistic-item-description'>Положительных отзывов</p>
            </div>
            <div className='statistic-item'>
                <p className='statistic-item-value'>13К+</p>
                <p className='statistic-item-description'>Счастливых пользователей</p>
            </div>
        </div>
    )
}

export default Statistic;