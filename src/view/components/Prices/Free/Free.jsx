import React from "react";

function Free(props) {
    const {price, period} = props;

    return (
        <div className='prices-item'>
            <div className='prices-item-header'>
                <p>Бесплатно</p>
                <p><span className='prices-value'>{price}$</span> / в месяц</p>
            </div>
            <div className='prices-item-body'>
                <p>{period}</p>
                <p>Неограниченный функционал</p>
                <p>Неограниченное число пользователей</p>
                <p>Интеграция с Instagram</p>
                <p>Интеграция с НовойПочтой</p>
            </div>
            <div className='prices-item-button'>
                <a className='btn-main' href='/try1'>Подключиться</a>
            </div>
        </div>
    )
}

export default Free;