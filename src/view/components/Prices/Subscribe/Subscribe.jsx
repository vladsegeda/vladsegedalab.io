import React from "react";

function Subscribe(props) {
    const {price, period} = props;

    return (
        <div className='prices-item'>
            <div className='prices-item-header'>
                <p>Подписка</p>
                <p><span className='prices-value'>{price}$</span> / в месяц</p>
            </div>
            <div className='prices-item-body'>
                <p>{period}</p>
                <p>Неограниченный функционал</p>
                <p>Неограниченное число пользователей</p>
                <p>Интеграция с Instagram</p>
                <p>Интеграция с НовойПочтой</p>
            </div>
            <div className='prices-item-button'>
                <a className='btn-main' href='/try2'>Подключиться</a>
            </div>
        </div>
    )
}

export default Subscribe;