import React from "react";
import Free from "./Free/Free";
import Subscribe from "./Subscribe/Subscribe";

function Prices() {
    return (
        <div className='prices'>
            <h1 className='prices-title'>Лучшие тарифные планы</h1>
            <p className='prices-text'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has<br/>been been the
                industry's standard dummy text ever since the 1500s.</p>
            <Free
                price='0'
                period='14 дней'
            />
            <Subscribe
                price='25'
                period='30 дней'
            />
        </div>
    )
}

export default Prices;