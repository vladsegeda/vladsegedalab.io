import React from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCode, faUserFriends, faLaptop, faCogs} from '@fortawesome/fontawesome-free-solid'

function TopFeatures() {
    return (
        <div className='topFeatures'>
            <div className='topFeatures-item'>
                <FontAwesomeIcon
                    className='topFeatures-item-icon'
                    size='3x'
                    color='#ed5106'
                    icon={faCode}/>
                <h3>Clear code</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div className='topFeatures-item'>
                <FontAwesomeIcon
                    className='topFeatures-item-icon'
                    size='3x'
                    color='#108cfc'
                    icon={faUserFriends}/>
                <h3>User Friendly </h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div className='topFeatures-item'>
                <FontAwesomeIcon
                    className='topFeatures-item-icon'
                    size='3x'
                    color='#c70e3a'
                    icon={faLaptop}/>
                <h3>Clean Interface</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div className='topFeatures-item'>
                <FontAwesomeIcon
                    className='topFeatures-item-icon'
                    size='3x'
                    color='#3c6e6a'
                    icon={faCogs}/>
                <h3>Ease Customize</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
        </div>
    )
}

export default TopFeatures;