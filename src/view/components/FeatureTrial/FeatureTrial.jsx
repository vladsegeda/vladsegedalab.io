import React from "react";

function FeatureTrial() {

    return(
        <div className='feature-trial'>
            <div><h1>Пробный период 7 дней</h1></div>
            <div><a className='btn-main-white' href='/try'>Подлючить</a></div>
        </div>
    )
};

export default FeatureTrial;