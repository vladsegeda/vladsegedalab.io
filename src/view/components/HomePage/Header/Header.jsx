import React from 'react';
import HelloText from "../HelloText/HelloText";
import bgImage from "../../../../static/img/header.png";

function Header() {
    return (
        <div className='home-header'>
            <div className='home-header-bg'>
                <img className='home-header-bg-image' src={bgImage} alt={bgImage} />
            </div>
            <div className='home-header-container'>
                <HelloText/>
            </div>
        </div>
    );
}

export default Header;
