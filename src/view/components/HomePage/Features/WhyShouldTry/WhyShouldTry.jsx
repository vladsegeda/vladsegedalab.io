import React from "react";
import why from "../../../../../static/img/why.png";

function WhyShouldTry() {
    return (
        <div className='home-features-container'>
            <div className='features-item'>
                <h2>Почему стоить попробовать MCRM?</h2>
                <p>Среди лидирующих на рынке CRM систем наш продукт занимает наиболее выгодную ценовую политику
                    при этом не отступая по качеству работы и функционалу.</p>
                <a className='features-item-btn' href='/about-us'>Узнать Больше</a>
            </div>
            <div className='features-item'>
                <div className='features-item-image'>
                    <img src={why} alt={why}/>
                </div>
            </div>
        </div>
    )
}

export default WhyShouldTry;