import React from 'react';
import opt from "../../../../../static/img/opt_proc.jpg";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faComments, faClipboardList, faChartArea} from '@fortawesome/fontawesome-free-solid'

function OptimazeProcesses() {
    return (
        <div className='home-features-container'>
            <div className='features-item'>
                <span className='features-item-why'>Почему выбирают нас</span>
                <h2 className='features-item-title'>Оптимизируй все процессы</h2>
                <div className='features-item-container'>
                    <div className='features-item-container-icon'>
                        <FontAwesomeIcon
                            className='opt-fa opt-fa-Comments'
                            size='2x'
                            inverse
                            icon={faComments}/>
                    </div>
                    <div className='features-item-container-text'>
                        <span>Общайся с клиентами</span>
                    </div>
                    <div className='features-item-container-icon'>
                        <FontAwesomeIcon
                            className='opt-fa opt-fa-ClipboardList'
                            size='2x'
                            inverse
                            icon={faClipboardList}/>
                    </div>
                    <div className='features-item-container-text'>
                        <span>Оформляй заказы</span>
                    </div>
                    <div className='features-item-container-icon'>
                        <FontAwesomeIcon
                            className='opt-fa opt-fa-ChartArea'
                            size='2x'
                            inverse
                            icon={faChartArea}/>
                    </div>
                    <div className='features-item-container-text'>
                        <span>Аналитика</span>
                    </div>
                </div>
            </div>
            <div className='features-item'>
                <img src={opt} alt={opt} className='features-item-opt'/>
            </div>
        </div>
    )
}

export default OptimazeProcesses;
