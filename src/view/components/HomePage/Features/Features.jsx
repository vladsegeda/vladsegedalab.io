import React from 'react';
import OptimazeProcesses from "./OptimizeProcesses/OptimizeProcesses";
import OurVirtues from "./OurVirtues/OurVirtues";
import WhyShouldTry from "./WhyShouldTry/WhyShouldTry";
import FeatureTrial from "../../FeatureTrial/FeatureTrial";
import ContactUs from "../../ContactUs/ContactUs";
import Prices from "../../Prices/Prices";
import Statistic from "../../Statistic/Statistic";
import TrustedBy from "../../TrustedBy/TrustedBy";
import TopFeatures from "../../TopFeatures/TopFeatures";
import LatestNews from "../../LatestNews/LatestNews";
import FAQBlock from "../../FAQ/FAQ";


function Features() {
    return (
        <div className='home-features'>
            <TrustedBy/>
            <TopFeatures/>
            <OurVirtues/>
            <WhyShouldTry/>
            <OptimazeProcesses/>
            <Statistic/>
            <Prices/>
            <FeatureTrial/>
            <ContactUs/>
            <LatestNews/>
            <FAQBlock />
        </div>
    );
}

export default Features;
