import React from "react";
import howItWorks from "../../../../../static/img/howitworks.jpg";

function OurVirtues() {
    return (
        <div className='home-features-container'>
            <div className='features-item'>
                <h2>Наши преимущества</h2>
                <p>Мы разработали уникальную CRM систему которая станет незаменимым помощником для владельцев и
                    менеджеров онлайн магазинов.</p>
                <p>— оставайся всегда на связи</p>
                <p>— оформляй заказы всего в несколько кликов</p>
                <p>— вся информация под рукой</p>
                <p>— список постоянных клиентов в одном месте</p>
                <a className='features-item-btn' href='/test'>Узнать Больше</a>
            </div>
            <div className='features-item'>
                <div className='features-item-image'>
                    <img src={howItWorks} alt={howItWorks}/>
                </div>
            </div>
        </div>
    )
}

export default OurVirtues;