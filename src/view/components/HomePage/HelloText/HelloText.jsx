import React from 'react';

function HelloText() {
    return (
        <div className='home-header-helloBlock'>
            <div className='home-header-item'>
                <h1>Самый простой способ <br /> сохранить свой бизнес</h1>
                <a className='btn-main' href='/about-us'>Подробнее о MCRM</a>
            </div>
        </div>
    );
}

export default HelloText;
