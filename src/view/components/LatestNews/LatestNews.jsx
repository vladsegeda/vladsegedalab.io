import React from "react";
import why from '../../../static/img/blog1.jpg';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCalendarAlt} from '@fortawesome/fontawesome-free-solid'
function LatestNews() {
    return (
        <div className='latestNews'>
            <h3 className='latestNews-title'>Последние новости</h3>
            <h1 className='latestNews-subTitle'>Хотите быть в курсе событий?</h1>
            <div className='latestNews-container'>
                <div className='latestNews-container-item'>
                    <div className='latestNews-container-image'><img src={why} alt={why}/></div>
                    <div className='latestNews-container-description'>
                        <p><FontAwesomeIcon
                            className='latestNews-container-icon'
                            size='1x'
                            color='#4f4f4f'
                            icon={faCalendarAlt}/>10 Апреля 2020</p>
                        <a href="/blog"><h4>Lorem ipsum dolor sit amet, consectetur adipiscing</h4></a>
                    </div>
                </div>
                <div className='latestNews-container-item'>
                    <div className='latestNews-container-image'><img src={why} alt={why}/></div>
                    <div className='latestNews-container-description'>
                        <p><FontAwesomeIcon
                            className='latestNews-container-icon'
                            size='1x'
                            color='#4f4f4f'
                            icon={faCalendarAlt}/>10 Апреля 2020</p>
                        <a href="/blog"><h4>Lorem ipsum dolor sit amet, consectetur adipiscing</h4></a>
                    </div>
                </div>
                <div className='latestNews-container-item'>
                    <div className='latestNews-container-image'><img src={why} alt={why}/></div>
                    <div className='latestNews-container-description'>
                        <p><FontAwesomeIcon
                            className='latestNews-container-icon'
                            size='1x'
                            color='#4f4f4f'
                            icon={faCalendarAlt}/>10 Апреля 2020</p>
                        <a href="/blog"><h4>Lorem ipsum dolor sit amet, consectetur adipiscing</h4></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LatestNews;