import React from 'react';
import { Router, Route } from "react-router-dom";

import Home from "../../Pages/Home/Home";
import {history} from "../../../utils/history";
import AboutUs from "../../Pages/AboutUs/AboutUs";
import HeaderMenu from "../HeaderMenu/HeaderMenu";
import Footer from "../Footer/Footer";
import Blog from "../../Pages/Blog/Blog";

function Routing() {

    return (
        <Router history={history} basename={process.env.PUBLIC_URL}>
            <HeaderMenu/>
            <Route exact path="/" component={Home} />
            <Route exact path="/about-us" component={AboutUs} />
            <Route exact path="/blog" component={Blog} />
            <Footer />
        </Router>
    )
}

export default Routing;
