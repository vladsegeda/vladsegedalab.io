import React from 'react';

function Footer() {
    return (
        <footer>
            <div className='container'>
                <div className='footer-item'>
                    <h1 className='logo'>MCRM</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a className="btn-main-white" href='/joinus'>Присоединиться к нам</a>
                </div>
                <div className='footer-item'>
                    <h3>О MCRM</h3>
                    <ul className='footer-navigation'>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/how-it-work"}>Как это работает</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/marketing"}>Цифровой маркетинг</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/service"}>Сервисы</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/security"}>Безопасность</a>
                        </li>
                    </ul>
                </div>
                <div className='footer-item'>
                    <h3>Информация</h3>
                    <ul className='footer-navigation'>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/team"}>Команда</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/pricing"}>Цены</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/about-us"}>О продукте</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/terms"}>Условия</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/faq"}>Вопрос Ответ</a>
                        </li>
                        <li className='footer-navigation-item'>
                            <a className="link" href={"/blog"}>Блог</a>
                        </li>
                    </ul>
                </div>
                <div className='footer-item'>
                    <h3>Поддержка</h3>
                    <p>Украина, Запорожье</p>
                    <a className="link" href="mail:support@mcrm.com">support@mcrm.com</a>
                </div>
            </div>
            <div className='copy'>
                <div className='copy-navigation'>
                    © {(new Date().getFullYear())} Все права защищены
                </div>
                <div className='copy-navigation'>
                    <a className="copy-navigation-link" href="/news">Новости</a>
                    <a className="copy-navigation-link" href="/contacts">Контакты</a>
                    <a className="copy-navigation-link" href="/policy">Политика конфиденциальности</a>
                </div>

            </div>
        </footer>
    );
}

export default Footer;
